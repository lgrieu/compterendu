const Name = document.getElementById("Name");
        const date = document.getElementById("Date");
        const System = document.getElementById("Sys");        
        
        const amelioration_opp = document.getElementById("amelioration_opp");        
        
        const design1 = document.getElementById("design1");
        const ref1 = document.getElementById("ref1");
        const Price1 = document.getElementById("Price1");
        const nb1 = document.getElementById("nb1");
        
        const design2 = document.getElementById("design2");
        const ref2 = document.getElementById("ref2");
        const Price2 = document.getElementById("Price2");
        const nb2 = document.getElementById("nb2");
        
        const design3 = document.getElementById("design3");
        const ref3 = document.getElementById("ref3");
        const Price3 = document.getElementById("Price3");
        const nb3 = document.getElementById("nb3");
        
        const PartsCost = document.getElementById("PartsCost");
        
        const Duree = document.getElementById("Duree");
        const UnaviableRate = document.getElementById("UnaviableRate");
        
        const workforceCost = document.getElementById("workforceCost"); 
        const DirectCost = document.getElementById("DirectCost");
        const IndirectCost = document.getElementById("IndirectCost");
        const MaintCost = document.getElementById("MaintCost");
         
    document.getElementById("ToPDF").addEventListener("click",CreatePDF);
    document.getElementById("amelioration").value = "amélioration =" + amelioration_opp.value;
    document.getElementById("amelioration").addEventListener("click", function() {this.value = "amélioration =" + amelioration_opp.value;});
    
    Price1.addEventListener("change",Calculate);
    nb1.addEventListener("change",Calculate);
    Price2.addEventListener("change",Calculate);
    nb2.addEventListener("change",Calculate);
    Price3.addEventListener("change",Calculate);
    nb3.addEventListener("change",Calculate);
    
    
    Duree.addEventListener("change",Calculate);
    UnaviableRate.addEventListener("change",Calculate);
    
    
    function Calculate() {
        let HoureRate = SelectedInput('HoureRate');
        let Total1 = parseFloat(Price1.value) * parseFloat(nb1.value);
        let Total2 = parseFloat(Price2.value) * parseFloat(nb2.value)
        let Total3 = parseFloat(Price3.value) * parseFloat(nb3.value)
        PartsCost.innerHTML = Total1 + Total2 + Total3; 
        workforceCost.innerHTML = parseFloat(HoureRate) * parseFloat(Duree.value);
        DirectCost.innerHTML = parseFloat(workforceCost.innerHTML) + parseFloat(PartsCost.innerHTML);
        IndirectCost.innerHTML = parseFloat(UnaviableRate.value)  * parseFloat(Duree.value);
        MaintCost.innerHTML = parseFloat(DirectCost.innerHTML) + parseFloat(IndirectCost.innerHTML);   
    }
    
    
    function CreatePDF() {
        var dd = {
	content: [
		
		{
			text: "Compte Rendu d'intervention",
			alignment: 'center',
			bold: true
			
		},

				
		{
			
			margin: [100, 5, 0, 15],
			table: {
				body: [
					[{ text: 'Nom :', bold: true, fillColor:'lightgrey'}, Name.value],
					[{ text: 'Date :', bold: true, fillColor:'lightgrey'}, date.value],
					[{ text: 'Système :', bold: true, fillColor:'lightgrey'}, System.value],
					[{ text: 'Type de maintenance :', bold: true, fillColor:'lightgrey'}, SelectedInput('MaintType')],
					[{ text: "Evènement à l'origine de l'intervention :", bold: true, fillColor:'lightgrey'}, SelectedInput('Event')],
					[{ text: "Type d'opération :", bold: true, fillColor:'lightgrey'}, SelectedInput('Opp')],
                    [{ text: "Niveau de maintenance :", bold: true, fillColor:'lightgrey'}, SelectedInput('MaintLevel')],
                    
                    [{ text: "Pièce commandée :", bold: true, fillColor:'lightgrey'}, design1.value],
                    [{ text: "Référence :", bold: true, fillColor:'lightgrey'}, ref1.value],
                    [{ text: "Prix :", bold: true, fillColor:'lightgrey'}, Price1.value],
                    [{ text: "Quantité :", bold: true, fillColor:'lightgrey'}, nb1.value],
                    
                    [{ text: "Pièce commandée :", bold: true, fillColor:'lightgrey'}, design2.value],
                    [{ text: "Référence :", bold: true, fillColor:'lightgrey'}, ref2.value],
                    [{ text: "Prix :", bold: true, fillColor:'lightgrey'}, Price2.value],
                    [{ text: "Quantité :", bold: true, fillColor:'lightgrey'}, nb2.value],                                        
                    
                    [{ text: "Pièce commandée :", bold: true, fillColor:'lightgrey'}, design3.value],
                    [{ text: "Référence :", bold: true, fillColor:'lightgrey'}, ref3.value],
                    [{ text: "Prix :", bold: true, fillColor:'lightgrey'}, Price3.value],
                    [{ text: "Quantité :", bold: true, fillColor:'lightgrey'}, nb3.value],
                    
                    [{ text: "Taux horaire :", bold: true, fillColor:'lightgrey'}, SelectedInput('HoureRate')],
                    [{ text: "Durée d'intervention :", bold: true, fillColor:'lightgrey'}, Duree.value],
                    [{ text: "Taux horaire d'indisponibilité :", bold: true, fillColor:'lightgrey'}, UnaviableRate.value],
                    [{ text: "Coût de maintenance :", bold: true, fillColor:'lightgrey'}, MaintCost.innerHTML],
                    [{ text: "Conclusion :", bold: true, fillColor:'lightgrey'}, SelectedInput('conclusion')],
                    
				]
			},
			//pageBreak: 'after'
		},
	],
    };       
        
        pdfMake.createPdf(dd).download();
    
    }
    let Selectors =  document.querySelectorAll("input[type= 'radio']");
    for (let i = 0; i < Selectors.length; i++) {
        Selectors[i].addEventListener("click", function() {
            SelectedInput(Selectors[i].name);
            Calculate();
            });
    }
    
    function SelectedInput(Name) {
        let input = document.getElementsByName(Name);
        let inputValue ="";
        for (let i = 0; i < input.length; i++) {
            if (input[i].type === 'radio' && input[i].checked) {
            inputValue = input[i].value;
            }
            //if (input[i].type === 'checkbox' && input[i].checked) {            
            //inputValue += input[i].value + "/";
            //}
        }
        return inputValue;
        
    }
SetDisplayedDivs();
document.getElementById('TypeChoice').addEventListener("click",SetDisplayedDivs);
    
function  SetDisplayedDivs() {
    switch (SelectedInput('MaintType')) {
        case 'Corrective':
            document.getElementById('EventChoice').style.display = 'block';
            document.getElementById('CorrEvents').style.display = 'block';
            document.getElementById('PrevEvents').style.display = 'none';
            
            document.getElementById('amelioration').checked = false;
            
            document.getElementById('CorrOpp').style.display = 'block';
            document.getElementById('PrevOpp').style.display = 'none';
            document.getElementById('AmelioOpp').style.display = 'none';
        break;
        case 'Preventive':
            document.getElementById('EventChoice').style.display = 'block';
            document.getElementById('CorrEvents').style.display = 'none';
            document.getElementById('PrevEvents').style.display = 'block';
            
            document.getElementById('amelioration').checked = false;
            
            document.getElementById('CorrOpp').style.display = 'none';
            document.getElementById('PrevOpp').style.display = 'block';
            document.getElementById('AmelioOpp').style.display = 'none';
        break;
        case 'Ameliorative':
        document.getElementById('EventChoice').style.display = 'none';
        
        document.getElementById('amelioration').checked = true;
        
        document.getElementById('CorrOpp').style.display = 'none';
        document.getElementById('PrevOpp').style.display = 'none';
        document.getElementById('AmelioOpp').style.display = 'block';
        break;
    };
}
        

      
